$(document).on('click', '#viewBookBtn', function () {
    $('#modal').modal('show').find('.modal-body').load($(this).attr('value'));
});

$(document).ready(function () {
    $(".simple").fancybox({
        helpers: {
            title: {
                type: 'over'
            }
        }
    });
});