<?php

use yii\helpers\Html;
use yii\grid\GridView;
use \yii\bootstrap\Modal;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

?>


<?
$this->title = 'Books';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="books-index">
	<?=$this->render('_search', ['model' => $searchModel]); ?>
	<div class="col-lg-12">
		<h1>
			<?= Html::encode($this->title) ?>
		</h1>

		<p>
			<?= Html::a('Create Books', ['create'], ['class' => 'btn btn-success']) ?>
		</p>
		<?php

		Modal::begin([
				'header' => ' Просмотр книги',
				'id' =>'modal',
				'size'=>'modal-lg'


			]);
		Modal::end();
		?>
		<?= GridView::widget([
				'dataProvider' => $dataProvider,
				// 'filterModel' => $searchModel,
				'columns' => [
					'id',
					'name',
					[
						'attribute' => 'preview',
						'format' => 'html',
						'value' =>
						function ($model)
						{
							return Html::a(Html::img($model->preview,['width' => '140px']),$model->preview,['class'=>'simple']);
						},
					],

					[
						'attribute' => 'authors',
						'value' =>
						function($model)
						{
							return $model->authors->firstname . ' ' . $model->authors->lastname;
						}
					],
					[
						'attribute' => 'date',
						'value' =>
						function($model)
						{
							if(!empty($model->date))
							return date('d.m.Y',$model->date);
						}
					],
					[
						'attribute' => 'date_create',
						'value' =>
						function($model)
						{

							return date('d.m.Y H:i:s',$model->date_create);
						}
					],
					[
						'attribute' => 'date_update',
						'value' =>
						function($model)
						{
							return date('d.m.Y H:i:s',$model->date_update);
						}
					],
					[
						'class' => \yii\grid\ActionColumn::className(),
						'buttons'=>[
							'view'=>
							function ($url,$model)
							{

								return Html::a('<span class="glyphicon glyphicon-eye-open"></span>','#',['value' => Url::to(['books/view','id'=>$model->id]),'id'=>'viewBookBtn']);
							}
						],
						'template'=>'{view}{update}{delete}',
					]
				],
			]); ?>
	</div>
</div>
