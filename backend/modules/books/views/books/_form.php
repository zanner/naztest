<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use \yii\helpers\ArrayHelper;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model backend\modules\books\models\Books */
/* @var $form yii\widgets\ActiveForm */
$model->date=date('d.m.Y',$model->date);
?>

<div class="books-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>



    <?= $form->field($model, 'icon')->fileInput(); ?>

<?= $form->field($model, 'date')->widget(DatePicker::className(), [


                       // 'value' => date('d.m.Y',$model->date_mer),
                        'language' => 'ru',

                        'options' => ['placeholder' => 'Select issue date ...'],
                        'pluginOptions' => [
                            'format' => 'dd.mm.yyyy',

                            'todayHighlight' => true
                        ]
                    ]); ?>

    <?= $form->field($model, 'author')->dropDownList(ArrayHelper::map(\backend\modules\books\models\Author::find()->all(),'id','lastname')); ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
