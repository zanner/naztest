<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model backend\modules\books\models\BooksSeach */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="books-search">
    <div class="col-lg-4">
    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>


<?php
echo '<span>Дата выхода книги:</span>';
echo DatePicker::widget([
    'name' => 'from_date',
    'value' => '',
    'type' => DatePicker::TYPE_RANGE,
    'name2' => 'to_date',
    'value2' => '',
    'pluginOptions' => [
        'autoclose'=>true,
        'format' => 'dd.mm.yyyy',
        'autoclose'=>true,
        'todayHighlight' => true
    ]
]);

?>
    <?= $form->field($model, 'name') ?>
    <?=$form->field($model, 'author')->dropDownList(ArrayHelper::map(\backend\modules\books\models\Author::find()->all(),'id','lastname'),['prompt' =>'Автор']); ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>
    </div>
</div>
