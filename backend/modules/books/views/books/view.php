<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\books\models\Books */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Books', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="books-view">

	<h1>
		<?= Html::encode($this->title) ?>
	</h1>


	<?= DetailView::widget([
			'model' => $model,
			'attributes' => [
				'id',
				'name',
				[
					'attribute' => 'date_create',
					'value' => date('d.m.Y H:i:s',$model->date_create)

				],
				[
					'attribute' => 'date_update',
					'value' => date('d.m.Y H:i:s',$model->date_update)

				],
				[
					'attribute' => 'preview',
					'format' => 'html',
					'value' => Html::img($model->preview,['width' => '140px'])
				],
				[
					'attribute' => 'date',
					'value' => date('d.m.Y',$model->date)

				],
				[
					'attribute' => 'authors',
					'value' => $model->authors->firstname . ' ' . $model->authors->lastname,

				],
			],
		]) ?>

</div>
