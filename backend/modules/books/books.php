<?php

namespace backend\modules\books;

class books extends \yii\base\Module
{
    public $controllerNamespace = 'backend\modules\books\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
