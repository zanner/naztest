<?php

namespace backend\modules\books\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "{{%books}}".
 *
 * @property string $id
 * @property string $name
 * @property string $date_create
 * @property string $date_update
 * @property string $preview
 * @property integer $date
 * @property integer $author
 */
class Books extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public $icon;
    public static function tableName()
    {
        return '{{%books}}';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'date_create',
                'updatedAtAttribute' => 'date_update',

            ],
        ];
    }
    public function rules()
    {
        return [
            [['date_create', 'date_update', 'author'], 'integer'],
            [['name'], 'string', 'max' => 250],
            [['date'], 'string', 'max' => 11],
            ['icon','file'                  ,
                'extensions'     =>'jpg, gif, png',
                'maxSize'   =>1024 * 1024 * 5,// 5 MB
                'skipOnEmpty'=>true,

            ],
        ];
    }



    public function getAuthors()
    {
        return $this->hasOne(Author::className(), ['id' => 'author']);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'date_create' => 'дата создания записи',
            'date_update' => 'дата обновления записи',
            'icon' => 'Картинка',
            'date' => 'дата выхода книги',
            'author' => ' ид автора в таблице авторы',
        ];
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
		$this->date = strtotime($this->date);

            return true;
        }
        return false;
    }
}
