<?php

namespace backend\modules\books\models;

use Yii;

/**
 * This is the model class for table "{{%auhor}}".
 *
 * @property string $id
 * @property string $firstname
 * @property string $lastname
 */
class Author extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%auhor}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['firstname', 'lastname'], 'string', 'max' => 50]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'firstname' => ' имя автора',
            'lastname' => 'фамилия автора',
        ];
    }
}
