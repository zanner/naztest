<?php

namespace backend\modules\books\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\books\models\Books;

/**
 * BooksSeach represents the model behind the search form about `backend\modules\books\models\Books`.
 */
class BooksSeach extends Books
{
    /**
     * @inheritdoc
     */
    public $authors;
    public $from_date;
    public $to_date;
    public function rules()
    {
        return [
            [['id', 'date_create', 'date_update'], 'integer'],
            [['name', 'preview', 'author','from_date','to_date'], 'safe'],
        ];
    }


    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Books::find();
       $query->joinWith(['authors']);



        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);


        $dataProvider->sort->attributes['authors'] = [
            // The tables are the ones our relation are configured to
            // in my case they are prefixed with "tbl_"
            'asc' => ['auhor.firstname' => SORT_ASC],
            'desc' => ['auhor.firstname' => SORT_DESC],
        ];





        if (!$this->validate()) {

                  // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'books.id' => $this->id,

            'author' => $this->author,
        ]);



        $query->andFilterWhere(['like', 'name', $this->name]);
        
         if(!empty($params['from_date'])){
                $query->andFilterWhere(['between', 'date', strtotime($params['from_date']), strtotime($params['to_date'])]);

            }

        return $dataProvider;
    }
}
