-- --------------------------------------------------------
-- Хост:                         127.0.0.1
-- Версия сервера:               5.5.45 - MySQL Community Server (GPL)
-- ОС Сервера:                   Win32
-- HeidiSQL Версия:              9.3.0.4984
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Дамп структуры базы данных test
CREATE DATABASE IF NOT EXISTS `test` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `test`;


-- Дамп структуры для таблица test.auhor
CREATE TABLE IF NOT EXISTS `auhor` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `firstname` char(50) DEFAULT NULL COMMENT ' имя автора',
  `lastname` char(50) DEFAULT NULL COMMENT 'фамилия автора',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы test.auhor: ~9 rows (приблизительно)
/*!40000 ALTER TABLE `auhor` DISABLE KEYS */;
INSERT INTO `auhor` (`id`, `firstname`, `lastname`) VALUES
	(1, 'Лев', 'Толстой'),
	(2, 'Тарас', 'Швеченко'),
	(3, 'Іван', 'Франко'),
	(4, 'Леся', 'Українка'),
	(5, 'Григорій', 'Тютюнник'),
	(6, 'Іван', 'Котляревський'),
	(7, 'Василь', 'Стус'),
	(8, 'Михайло', 'Коцюбинський'),
	(9, 'Олександр', 'Довженко');
/*!40000 ALTER TABLE `auhor` ENABLE KEYS */;


-- Дамп структуры для таблица test.books
CREATE TABLE IF NOT EXISTS `books` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(250) DEFAULT NULL,
  `date_create` int(11) unsigned DEFAULT NULL COMMENT 'дата создания записи',
  `date_update` int(11) unsigned DEFAULT NULL COMMENT 'дата обновления записи',
  `preview` varchar(250) DEFAULT NULL COMMENT 'путь к картинке превью книги',
  `date` int(11) DEFAULT NULL COMMENT 'дата выхода книги',
  `author` int(11) DEFAULT NULL COMMENT ' ид автора в таблице авторы',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы test.books: ~4 rows (приблизительно)
/*!40000 ALTER TABLE `books` DISABLE KEYS */;
INSERT INTO `books` (`id`, `name`, `date_create`, `date_update`, `preview`, `date`, `author`) VALUES
	(9, '12311', 1453463339, 1453557634, '/admin/images/3ea.jpg', 1454706000, 1),
	(10, '123dfgdfdfgdfg', 1453463375, 1453556593, NULL, 1453237200, 1),
	(22, 'Тестова книга', 1453478232, 1453557820, '/admin/images/3ea.jpg', 1453237200, 3),
	(24, 'sdfsdfsdf', 1453556031, 1453556541, NULL, 1454101200, 8);
/*!40000 ALTER TABLE `books` ENABLE KEYS */;


-- Дамп структуры для таблица test.user
CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `auth_key` varchar(32) DEFAULT NULL,
  `email_confirm_token` varchar(255) DEFAULT NULL,
  `password_hash` varchar(255) NOT NULL,
  `password_reset_token` varchar(255) DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `role` varchar(1) NOT NULL,
  `status` smallint(6) NOT NULL DEFAULT '0',
  `fname` varchar(255) DEFAULT NULL,
  `lname` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_user_username` (`username`),
  KEY `idx_user_email` (`email`),
  KEY `idx_user_status` (`status`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы test.user: ~1 rows (приблизительно)
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` (`id`, `created_at`, `updated_at`, `username`, `auth_key`, `email_confirm_token`, `password_hash`, `password_reset_token`, `email`, `role`, `status`, `fname`, `lname`) VALUES
	(1, 0, 0, 'naz', 'nHzaEA1EVn8orDpXwfmgvjDWm1YdBKsB', NULL, '$2y$13$Z.tVlIg8LkcUW5kkYbTg.ODcg/fc4.VrpWBowltTVr.RV6E.2woPO', NULL, '', '', 0, NULL, NULL);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
